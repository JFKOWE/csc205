Introduction Notes:
The Altair was the first available home computer both in pre-assembled and in kit form.

It was designed for people with no computer experience.

Logic Notes:
in 1847, Boole published a book, relating mathematics and logic. This was later developed into "Boolean Algebra" add text

Using only ON or OFF conditions, logic gates can be formed to represent AND, OR, and NOT.

Tables can be made to represent AND OR and NOT gates

Electronic Logic:
There are symbols used to represent AND OR and NOT.

Gates can be combined, and the most common combinations are NAND and NOR. These gates have their own symbols.

Three or more logic circuits make a logic system.


Number Systems:
early humans devised a number system which consisted of ten digits

Systems based upon eight and sixteen provide a convenient shorthand method for expressing lengthy binary numbers.

The Binary System:
the ALTAIR 8800 performs nearly all operations in binary

Counting in Binary is very simple

The Octal System:
The basic ALTAIR 8800 accepts a binary input, and any binary number loaded into the machine can be simplified into octal format.

Computer Programming:
The software instructions for the ALTAIR 8800 must be loaded into the machine in the form of sequential 8-bit words called machine language.

Frequently used special purpose programs can be stored in the computer’s memory for later retrieval and use by the main program. Such a special purpose program is called a subroutine.


Watching a Video Series about the Altair notes:
https://www.youtube.com/watch?v=7nDcTRqZu8E&list=PLgpana-oqo-KSE-Hlc3uLJ9xISCByBpO1

The two, three, three lights on the top of the Altair refer to the 8 bits in a byte, allowing for easy octal translation.

The jump command brings the program to a certain place in the Altair's memory

Commands are keyed in using Octal

The examine switch shows you what is in the keyed in memory location

Practiced changing from binary to hex

FINALLY UNDERSTAND THE SAMPLE PROGRAM!!!


Silas and I worked together to do the first 2 weekly tasks today but we failed to make any commits because we were caught up in trying to make it work. We learned how to program the Altair today which was really good.


Notes from 9/15/2022:

Reading through all machine language codes to hopefully find something to allow us to compare two numbers. Right now I am reading section 4 of Ubuntourist's manual port.

CMP compares register with accumulator

Signed bits are just bits for interpretation
