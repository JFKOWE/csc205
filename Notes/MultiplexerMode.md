# What is a multiplexer circuit?

- The multiplexer, shortened to “MUX” or “MPX”, is a combinational logic circuit designed to switch one of several input lines through to a single common output line by the application of a control signal.
---
## Lets break this down

![alt text](https://www.electronics-tutorials.ws/wp-content/uploads/2018/05/combination-multiplexer2.gif)

- The multiplexer, shortened to “MUX” or “MPX”, is a combinational logic circuit designed to switch one of several input lines through to a single common output line by the application of a control signal.
- The image above is the simplest multiplexer we can make. With "x" number of selector swithes, we can make a multiplexer with 2^x inputs.

### For Example

![alt text](https://www.electronics-tutorials.ws/wp-content/uploads/2018/05/combination-multiplexer3.gif)

![alt text](https://www.electronics-tutorials.ws/wp-content/uploads/2018/05/combination-comb1a.gif)
---

## What Can These Be Used For?

- Multiplexers are used in computer memory to reduce the number of copper lines required to connect the memory to other parts of the computer.

- In telephone networks, multiple audio signals are integrated on a single line of transmission with the help of a multiplexer.

- The multiplexer is used to transmit the data signals from the computer system of a spacecraft or a satellite to the ground system by using a GSM satellite.
