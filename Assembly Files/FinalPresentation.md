# JFK and Silas Spectacular (-ly nonfunctional) Reversing Program

- The goal of my program was to have the output be the reverse of the user's input. This would be achieved by moving the characters of the input buffer to the stack, then returning them in reverse order. This presented numerous challenges for someone still learning assembly.
---
## What went wrong in each version?

Version One Issues:
- Missing input buffer pointer initialization
- Missing the input buffer length calculation
- Missing the input buffer pointer adjustment after the input

Version Two Issues:
- Missing the initialization of the HL register pair with the address of the input buffer before calling the REVERSE_ECHO function, so the REVERSE_ECHO function does not know where the input buffer is located.
- In the CO function, it is missing the instruction to move the input buffer pointer to the next character after outputting the current character, so the next character will not be output.

Version Three Issues:
- There's an issue with the return instruction in the COMSG function, the RZ instruction is not a valid instruction for the Intel 8080, it should be RET instruction.

Version Four/Five Issues:
- Finally realized an issue with the null pointer being used. When reversed, the null pointer is placed at the beginning of the print, terminating the program early.
- Post This version, a decrementing method was used.

Version Six Issues:
- The problem with the last version of the program is that it's using the wrong register to store the buffer length. Instead of storing the buffer length in the DE register, it's storing the buffer pointer in DE. And the program is incrementing the pointer instead of the buffer length.

Version Seven Issues:

- Does not compile due to a load error resulting from attempting a new way of initializing the program

### What Should We Have Done Differently?

If we were to do this project again, we would not start by building off an existing program, we would try to make one from scratch. This would of course come with its own difficulties but would resolve some issues that were experienced with logic flow. We would also try to figure out a less convoluted way for the program to function, and maybe this would result in a working program.
---

## What Did We learn?

- A multitude of Assembly Language lessons about which instructions are valid for the Intel 8080 processor

- The value of quality reference material

- We learned more about how programs are actually stored in memory by experiencing it first hand


- Better commenting habits to increase code readability.

